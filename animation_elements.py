#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#  Animation Elements - An Inkscape 1.1+ extension -->
#  Just a quick way to make animation elements in Inkscape -->


import inkex
from inkex import Transform

from lxml import etree

# Import Klowner applytransform
# Copyright Mark "Klowner" Riedesel
# https://github.com/Klowner/inkscape-applytransforms
import applytransform_edited
# End of Klowner import

def get_attributes(self):
    for att in dir(self):
        try:
            inkex.errormsg((att, getattr(self, att)))
        except:
            None


def get_inverse_transform(self, transform):
    import numpy
    my_object_np_matrix = list(transform.matrix)
    my_object_np_matrix.append((0, 0, 1))

    inverse_matrix = numpy.linalg.inv(my_object_np_matrix)

    inkscape_inverse_matrix = inverse_matrix.tolist()
    inkscape_inverse_matrix.pop(2)

    inverse_transform = Transform().add_matrix(inkscape_inverse_matrix)

    return inverse_transform

def add_animate_motion(self, my_object, path):

    parent = my_object

    animate_motion_elements = my_object.xpath('./svg:animateMotion')
    for item in animate_motion_elements:
        item.delete()

    # Lets take my object back to 0,0

    my_object_origin_transform = Transform()

    parent_transform = my_object.getparent().composed_transform()

    my_object_bbox = my_object.bounding_box(True)

    # inkex.errormsg(my_object_bbox)

    inverse_transform = get_inverse_transform(self, my_object.transform)

    my_object_origin_translate_x = - my_object_bbox.left - my_object_bbox.width / 2
    my_object_origin_translate_y = - my_object_bbox.top - my_object_bbox.height / 2

    # my_object_origin_transform = inverse_transform @ my_object_origin_transform

    my_object_origin_transform.add_translate(my_object_origin_translate_x, my_object_origin_translate_y)

    # Parent composed transform is required because layers(groups) may have transforms

    if self.options.reset_origin_cb == 'true':

        my_object.transform = my_object_origin_transform @ my_object.transform
        path.transform = path.transform @ my_object.getparent().composed_transform()
        path_index = path.xml_path
        processed_shape = applytransform_edited.effect(self.svg.selection, path_index, path)
        path = processed_shape

    else:

        path.transform = my_object_origin_transform @ my_object.getparent().composed_transform()

        path_index = path.xml_path

        processed_shape = applytransform_edited.effect(self.svg.selection, path_index, path)

        path = processed_shape

        path.style['fill'] = 'grey'


    am_attrib_dict = {}

    if len(my_object.xpath('./svg:animateMotion')) > 0:
        inkex.errormsg('animateMotion Element Found')
        return
    else:
        animate_motion = etree.SubElement(parent, inkex.addNS('animateMotion', 'svg'))

        if self.options.am_keypoints_cb == 'true': am_attrib_dict['keyPoints'] = self.options.am_keypoints_float

        if self.options.am_rotate_cb == 'true':
            if self.options.am_rotate_combo == 'number':
                am_attrib_dict['rotate'] = self.options.am_rotate_float
            else:
                am_attrib_dict['rotate'] = self.options.am_rotate_combo

        if self.options.am_begin_cb == 'true': am_attrib_dict['begin'] = self.options.am_begin_float
        if self.options.am_dur_cb == 'true': am_attrib_dict['dur'] = self.options.am_dur_float
        if self.options.am_end_cb == 'true': am_attrib_dict['end'] = self.options.am_end_float
        if self.options.am_min_cb == 'true': am_attrib_dict['min'] = self.options.am_min_float
        if self.options.am_max_cb == 'true': am_attrib_dict['max'] = self.options.am_max_float
        if self.options.am_restart_cb == 'true': am_attrib_dict['restart'] = self.options.am_restart_combo

        if self.options.am_repeatcount_cb == 'true':
            if self.options.am_repeatcount_int == -1:
                am_attrib_dict['repeatCount'] = 'indefinite'
            else:
                am_attrib_dict['repeatCount'] = self.options.am_repeatcount_int

        if self.options.am_repeatdur_cb == 'true': am_attrib_dict['repeatDur'] = self.options.am_repeatdur_combo
        if self.options.am_fill_cb == 'true': am_attrib_dict['fill'] = self.options.am_fill_combo
        if self.options.am_calcmode_cb == 'true': am_attrib_dict['calcMode'] = self.options.am_calcmode_combo
        if self.options.am_values_cb == 'true': am_attrib_dict['values'] = self.options.am_values_string
        if self.options.am_keytimes_cb == 'true': am_attrib_dict['keyTimes'] = self.options.am_keytimes_string
        if self.options.am_keysplines_cb == 'true': am_attrib_dict['keySplines'] = self.options.am_keysplines_string
        if self.options.am_from_cb == 'true': am_attrib_dict['from'] = self.options.am_from_float
        if self.options.am_to_cb == 'true': am_attrib_dict['to'] = self.options.am_to_float
        if self.options.am_by_cb == 'true': am_attrib_dict['by'] = self.options.am_by_float
        if self.options.am_additive_cb == 'true': am_attrib_dict['additive'] = self.options.am_additive_combo
        if self.options.am_accumulate_cb == 'true': am_attrib_dict['accumulate'] = self.options.am_accumulate_combo

        # inkex.errormsg(am_attrib_dict)

        for key, value in am_attrib_dict.items():
            animate_motion.set(key, value)

# my_object_path = path.get('d')

        path_absolute = path.path.to_absolute()

        # my_object.transform = path.getparent().composed_transform() @ parent_transform

        animate_motion.set('path', path_absolute)
        # animate_motion.set('path', path.path)



class AnimationElements(inkex.EffectExtension):

    def add_arguments(self, pars):



        pars.add_argument("--animation_elements_notebook", type=str, dest="animation_elements_notebook", default=0)

        pars.add_argument("--reset_origin_cb", type=str, dest="reset_origin_cb")

        pars.add_argument("--am_keypoints_cb", type=str, dest="am_keypoints_cb", default="false")
        pars.add_argument("--am_keypoints_string", type=str, dest="am_keypoints_string", default='x')

        pars.add_argument("--am_rotate_cb", type=str, dest="am_rotate_cb", default="false")
        pars.add_argument("--am_rotate_combo", type=str, dest="am_rotate_combo", default='always')
        pars.add_argument("--am_rotate_float", type=float, dest="am_rotate_float", default=0)

        pars.add_argument("--am_begin_cb", type=str, dest="am_begin_cb", default="false")
        pars.add_argument("--am_begin_float", type=float, dest="am_begin_float", default=0)

        pars.add_argument("--am_dur_cb", type=str, dest="am_dur_cb", default="false")
        pars.add_argument("--am_dur_float", type=float, dest="am_dur_float", default=0)

        pars.add_argument("--am_end_cb", type=str, dest="am_end_cb", default="false")
        pars.add_argument("--am_end_float", type=float, dest="am_end_float", default=0)

        pars.add_argument("--am_min_cb", type=str, dest="am_min_cb", default="false")
        pars.add_argument("--am_min_float", type=float, dest="am_min_float", default=0)

        pars.add_argument("--am_max_cb", type=str, dest="am_max_cb", default="false")
        pars.add_argument("--am_max_float", type=float, dest="am_max_float", default=0)

        pars.add_argument("--am_restart_cb", type=str, dest="am_restart_cb", default="false")
        pars.add_argument("--am_restart_combo", type=str, dest="am_restart_combo", default='always')

        pars.add_argument("--am_repeatcount_cb", type=str, dest="am_repeatcount_cb", default="false")
        pars.add_argument("--am_repeatcount_int", type=int, dest="am_repeatcount_int", default=1)

        pars.add_argument("--am_repeatdur_cb", type=str, dest="am_repeatdur_cb", default="false")
        pars.add_argument("--am_repeatdur_float", type=float, dest="am_repeatdur_float", default=1)

        pars.add_argument("--am_fill_cb", type=str, dest="am_fill_cb", default="false")
        pars.add_argument("--am_fill_combo", type=str, dest="am_fill_combo", default='remove')

        pars.add_argument("--am_calcmode_cb", type=str, dest="am_calcmode_cb", default="false")
        pars.add_argument("--am_calcmode_combo", type=str, dest="am_calcmode_combo", default='remove')

        pars.add_argument("--am_values_cb", type=str, dest="am_values_cb", default="false")
        pars.add_argument("--am_values_string", type=str, dest="am_values_string", default='x')

        pars.add_argument("--am_keytimes_cb", type=str, dest="am_keytimes_cb", default="false")
        pars.add_argument("--am_keytimes_string", type=str, dest="am_keytimes_string", default='x')

        pars.add_argument("--am_keysplines_cb", type=str, dest="am_keysplines_cb", default="false")
        pars.add_argument("--am_keysplines_string", type=str, dest="am_keysplines_string", default='x')

        pars.add_argument("--am_from_cb", type=str, dest="am_from_cb", default="false")
        pars.add_argument("--am_from_float", type=float, dest="am_from_float", default=1)

        pars.add_argument("--am_to_cb", type=str, dest="am_to_cb", default="false")
        pars.add_argument("--am_to_float", type=float, dest="am_to_float", default=1)

        pars.add_argument("--am_by_cb", type=str, dest="am_by_cb", default="false")
        pars.add_argument("--am_by_float", type=float, dest="am_by_float", default=1)

        pars.add_argument("--am_additive_cb", type=str, dest="am_additive_cb", default="false")
        pars.add_argument("--am_additive_combo", type=str, dest="am_additive_combo", default='remove')

        pars.add_argument("--am_accumulate_cb", type=str, dest="am_accumulate_cb", default="false")
        pars.add_argument("--am_accumulate_combo", type=str, dest="am_accumulate_combo", default='remove')

    def effect(self):

        selection_list = self.svg.selection

        # first_object = selection_list[0]
        # inkex.errormsg(first_object.xml_path)
        #
        # return

        if len(selection_list) != 2:
            inkex.errormsg('Please select two objects')
            return

        my_object = selection_list[0]
        my_path = selection_list[1]

        second_object_index = list(selection_list.items())[1]
        second_object = list(selection_list.items())[1][1]

        # inkex.errormsg(selection_list[1].get('d'))

        processed_shape = applytransform_edited.effect(selection_list, second_object_index, second_object)


        # inkex.errormsg(f'processed shape {processed_shape.get("d")}')

        notebook_page = self.options.animation_elements_notebook

        # return

        if notebook_page == 'animate_motion_page':
            # from copy import deepcopy
            # new_self = deepcopy(self)

            add_animate_motion(self, my_object, processed_shape)

        # inkex.errormsg(my_object.composed_transform().matrix)

if __name__ == '__main__':
    AnimationElements().run()

