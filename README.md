# Animation Elements

Just a simple extension to make creating `<animateMotion>` elements a bit simpler in Inkscape

Not all options make sense when combined.

For objects / path in the same layer:

Able to create a motion point without moving the object origin ( except for 'rotate' attribute which still requires 0,0 )

Uses apply transforms : Copyright Mark "Klowner" Riedesel https://github.com/Klowner/inkscape-applytransforms

To flatten the motion path.
